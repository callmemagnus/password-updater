const GRACE_TIME = 30 * 60 * 1000;
const fallbackCache = {};

export function read<T>(key: string, defaultValue: T): T {
  const now = Date.now();
  try {
    const data = JSON.parse(sessionStorage.getItem(key));
    if (!data || data.timestamp + GRACE_TIME < now) {
      sessionStorage.removeItem(key);
      return defaultValue;
    }
    return data.value;
  } catch (e) {
    if (fallbackCache[key]) {
      return fallbackCache[key];
    }
    return defaultValue;
  }
}

export function write<T>(key: string, value: T): void {
  try {
    sessionStorage.setItem(key, JSON.stringify({ timestamp: Date.now(), value }));
  } catch (e) {
    // ignore
    console.log("Write in sessionStorage failed.", e);
  }
  fallbackCache[key] = value;
}
