import { writable } from "svelte/store";
import { read, write } from "./storage";

export const currentLanguage = writable<string>(read("language", "EN"));
export const session = writable<Session>(read("session", {}));

interface Session {
  token?: string;
  email?: string;
}

currentLanguage.subscribe((language) => write("language", language));
session.subscribe((session) => write<Session>("session", session));

window.addEventListener("unhandledrejection", (e) => {
  console.log("BOOM", e);
  sessionStorage.clear();
});
