declare var baseUrl: string;

const matchDuplicateRegex = /\/+/g;

console.log(`Using "${baseUrl}" as base url for all calls`);

export default function addBaseUrl(service) {
  // @ts-ignore
  return `${baseUrl}/${service}`.replaceAll(matchDuplicateRegex, "/");
}
