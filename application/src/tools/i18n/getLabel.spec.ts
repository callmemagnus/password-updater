import test from "tape";

import getLabel from "./getLabel";

const labels = {
  noSubs: "test",
  oneSubs: "test {} test",
  twoSubs: "test {} test {} test",
};

test("getLabel", (assert) => {
  assert.equal(getLabel(labels, "oneSubs", "REPLACEMENT"), "test REPLACEMENT test", "label is returned correctly");

  assert.equal(
    getLabel(labels, "twoSubs", "REPLACEMENT1", "REPLACEMENT2"),
    "test REPLACEMENT1 test REPLACEMENT2 test",
    "label with 2 placeholders is returned correctly"
  );

  assert.equal(
    getLabel(labels, "noSubs", "REPLACEMENT1", "REPLACEMENT2"),
    "test",
    "label without placeholders ignores the substitutions"
  );
  assert.end();
});
