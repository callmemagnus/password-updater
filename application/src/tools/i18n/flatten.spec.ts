import test from "tape";
import flatten from "./flatten";

const json = {
  str: "this is a label",
  obj: {
    a: "a label",
    b: "b label",
  },
  oneSubs: "test {} test",
  twoSubs: "test {} test {} test",
};

test("flatten", (assert) => {
  const result = flatten(json);

  assert.equal(result["str"], json.str, "works with a string");
  assert.equal(result["obj.a"], json.obj.a, "works with an object");
  assert.end();
});
