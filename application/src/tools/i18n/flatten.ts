// Yes I could use lodash or equivalent

/**
 *
 * @param {Object} obj
 * @returns one level keyed object
 */
export default function flatten(obj) {
  function subflatten(accumulator, current, subobj) {
    for (const key in subobj) {
      if (subobj.hasOwnProperty(key)) {
        // we don't check the fact that it is a string
        const newKey = `${current}${key}`;
        if (typeof subobj[key] === "object") {
          subflatten(accumulator, `${newKey}.`, subobj[key]);
        } else {
          accumulator[newKey] = subobj[key];
        }
      }
    }
    return accumulator;
  }

  return subflatten({}, "", obj);
}
