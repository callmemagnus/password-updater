export default function getLabel(translations: Record<string, string>, key: string, ...subs: string[]): string {
  if (!translations) {
    // when translations have not been downloaded yet
    return "";
  }

  const label = translations[key];

  if (!label) {
    console.error(`Missing label: ${key}`);
    return "";
  }

  if (subs.length) {
    return subs.reduce((acc, sub) => {
      return acc.replace(/{}/, sub);
    }, label);
  }

  return label;
}
