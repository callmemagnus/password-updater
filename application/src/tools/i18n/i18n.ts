import { writable } from "svelte/store";

import flatten from "./flatten";
import getLabel from "./getLabel";

import { currentLanguage } from "../stores";
import { getLabels } from "../api";

const { subscribe, set } = writable(getLabel.bind(null, null));

export const isReady = writable(false);

function newLocaleHandler(newLocale) {
  console.log("updating language");

  return getLabels(newLocale.toLowerCase())
    .then((labels) => {
      set(getLabel.bind(null, flatten(labels)));
      isReady.set(true);
    })
    .catch((e) => {
      // @ts-ignore
      if (process.env.NODE_ENV !== "production") {
        console.error(e);
      }
    });
}

currentLanguage.subscribe(newLocaleHandler);

export default { subscribe, isReady };
