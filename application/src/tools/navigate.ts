import { navigate as routingNavigate } from "svelte-routing";
import addBaseUrl from "./baseUrl";

export function navigate(newPath, options = {}) {
  routingNavigate(addBaseUrl(newPath), options);
}
