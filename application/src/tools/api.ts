import { get } from "svelte/store";
import { navigate } from "./navigate";
import { session } from "./stores";
import addBaseUrl from "./baseUrl";

let pingTimer = null;

startPing();

export function startPing() {
  clearInterval(pingTimer);
  pingTimer = setInterval(ping, 1000);
}

export function login(username, password) {
  // obtain update token and save it locally
  return fetch(addBaseUrl("/api/login"), {
    method: "post",
    headers: { "content-type": "application/json" },
    body: JSON.stringify({ username, password }),
  })
    .then((response) => {
      if (response.status !== 200) {
        return Promise.reject();
      }
      return response.json();
    })
    .then(({ token }) => {
      if (!token) {
        return Promise.reject();
      }
      session.update((previous) => ({ ...previous, token }));
      startPing();
      return Promise.resolve();
    });
}

export function logout() {
  navigate("/login");
  clearInterval(pingTimer);
}

export function updatePassword(newPassword) {
  // first we request the update, then we poll with the request id and the token
  return fetch(addBaseUrl("/api/password"), {
    method: "post",
    body: JSON.stringify({
      newPassword,
    }),
    headers: {
      authorization: `bearer ${get(session).token}`,
      "content-type": "application/json",
    },
  }).then((response) => {
    if (response.status !== 200) {
      return Promise.reject();
    }
    return Promise.resolve();
  });
}

export function ping() {
  return fetch(addBaseUrl("/api/ping"), {
    headers: {
      authorization: `bearer ${get(session).token}`,
    },
  })
    .then((response) => {
      if (response.status !== 200) {
        throw new Error("Token probably expired.");
      }
    })
    .catch((e) => {
      console.log("Ping failure");
      clearInterval(pingTimer);
      session.set({});
    });
}

export function getLabels(locale) {
  return fetch(addBaseUrl(`/assets/${locale}.json`)).then((response) => {
    if (!response.ok) {
      throw new Error(`Error while trying to fetch ${locale}: ${response.status} ${response.statusText}.`);
    }
    return response.json();
  });
}
