import svelte from "rollup-plugin-svelte";
import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import livereload from "rollup-plugin-livereload";
import { terser } from "rollup-plugin-terser";
import replace from "@rollup/plugin-replace";
import sveltePreprocess from "svelte-preprocess";
import typescript from "@rollup/plugin-typescript";
import html from "@rollup/plugin-html";
import copy from "rollup-plugin-copy";
import css from "rollup-plugin-css-only";

const production = !process.env.ROLLUP_WATCH;

function serve() {
  let server;

  function toExit() {
    if (server) server.kill(0);
  }

  return {
    writeBundle() {
      if (server) return;
      server = require("child_process").spawn("npm", ["run", "start", "--", "--dev"], {
        stdio: ["ignore", "inherit", "inherit"],
        shell: true,
      });

      process.on("SIGTERM", toExit);
      process.on("exit", toExit);
    },
  };
}

console.log("production", production);

export default {
  input: "src/main.ts",
  //dest: "public/build/bundle-[hash].js",
  output: {
    sourcemap: true,
    format: "iife",
    name: "app",
    file: "public/main.js",
  },
  plugins: [
    svelte({
      compilerOptions: {
        // enable run-time checks when not in production
        dev: !production,
      },
      preprocess: sveltePreprocess(),
    }),
    // we'll extract any component CSS out into
    // a separate file - better for performance
    css({ browser: true, output: "bundle.css" }),

    // If you have external dependencies installed from
    // npm, you'll most likely need these plugins. In
    // some cases you'll need additional configuration -
    // consult the documentation for details:
    // https://github.com/rollup/plugins/tree/master/packages/commonjs
    resolve({
      browser: true,
      dedupe: ["svelte"],
    }),
    commonjs(),
    typescript({ sourceMap: !production }),

    replace({
      preventAssignment: true,
      __API_HOST__: process.env.API_HOST ? process.env.API_HOST : "",
      include: "src/**/*{.ts,.svelte}",
    }),

    // In dev mode, call `npm run start` once
    // the bundle has been generated
    //!production && serve(),

    // Watch the `public` directory and refresh the
    // browser on changes when not in production
    !production && livereload("public"),
    production && terser(),
    html({
      template: ({ attributes, bundle, files, meta, publicPath, title }) => {
        const now = Date.now();
        return `<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Password updater</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/global.css">
    ${files.css.map((file) => `<link rel="stylesheet" href="/${file.fileName}?${now}">`)}
  </head>
    
  <body>
    <script>const baseUrl="/";</script>
    ${files.js.map((file) => `<script src="/${file.fileName}?${now}"></script>`)}
  </body>
</html>`;
      },
    }),
    copy({
      targets: [
        { src: "src/assets/**/*", dest: "public/assets" },
        { src: "src/global.css", dest: "public" },
      ],
    }),
  ],
  watch: {
    clearScreen: false,
  },
};
