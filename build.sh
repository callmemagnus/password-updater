#!/bin/bash

IMAGE=test/password-updater
CONTAINER=passwordupdate

function title {
  echo ""
  echo ""
  echo "*************************************************"
  echo "** $1"
  echo "*************************************************"
  echo ""
}

function log_and_exit {
  echo "$1"
  exit $2
}

# clean up
docker stop $CONTAINER
docker rm $CONTAINER
docker rmi $IMAGE --force
rm -r domain/fixtures/accounts.txt

title "Running tests"

# global setup
npm install && npm run setup || log_and_exit "Install on / failed" 1
# frontend tests and lint
pushd domain && npm test && npm run lint && popd || log_and_exit "Install in /domain failed" exit 1
# backend test
pushd application && npm test && popd || log_and_exit "Install in /application failed" 1


title "Building the image"

docker build -t $IMAGE . || log_and_exit "Building the image locally failed" 1

rm -r domain/fixtures/accounts.txt
cp ./domain/fixtures/accounts.txt{.orig,}

npm ci

title "Running the tests with a BASE_PATH"
rm -r domain/fixtures/accounts.txt
cp ./domain/fixtures/accounts.txt{.orig,}
docker run \
	-v "$(pwd)/domain/fixtures":/data \
	-e ACCOUNTS_FILE=/data/accounts.txt \
	-e DOMAINS=test.local \
	-e BASE_PATH=/change/password \
	-p8080:8080 \
	--rm \
	--name $CONTAINER \
	-d \
	$IMAGE || log_and_exit "/change/password starting container failed" 1

npx wait-on -d 1000 -t 10000 -v http://localhost:8080/change/password || exit 1
echo '{"baseUrl": "http://localhost:8080/change/password"}' > cypress.json
npx cypress run || log_and_exit "/change/password starting container tests failed" 1

docker container stop $CONTAINER

title "Running the tests without a BASE_PATH"
rm -r domain/fixtures/accounts.txt
cp ./domain/fixtures/accounts.txt{.orig,}
docker run \
	-v "$(pwd)/domain/fixtures":/data \
	-e ACCOUNTS_FILE=/data/accounts.txt \
	-e DOMAINS=test.local \
	-p8080:8080 \
	--rm \
	--name $CONTAINER \
	-d \
	$IMAGE || log_and_exit "Starting container with / failed" 1

npx wait-on -d 1000 -t 10000 -v http://localhost:8080/ || exit 1

echo '{"baseUrl": "http://localhost:8080/"}' > cypress.json
npx cypress run || log_and_exit "/ starting container tests failed" 1
docker container stop $CONTAINER

git checkout cypress.json