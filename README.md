# Password updater app

This app provides a web interface to change the password of a system having passwords stored in the following format:

```
whatever.this.could.be@there.local|{SHA512-CRYPT}$6$T9hJL9D71tmXteqk$y9wMJrLvDlOpNiTQ.jRlS348b/7Ssx6sUH5y5eRMcijgtIzQnACzcYPyZdLn2L6YR3NagUAIvIgouwKdQ825G/
```

Frontend is a built on top of [svelte3](https://svelte.dev/) framework.

Backend is a based on [hapijs](https://hapijs.com/).

It is built to be a "companion" to the `mailserver` docker image.

## Frontend (application folder)

To develop: `npm run serve`.

To build the frontend `npm run build`, it will create the webapp in the `public` folder at root level.

To build continuously: `npm run dev`. You'll need either an http-server (e.g. spa-http-server) or the running backend.

## Backend (domain folder)

To develop: `npm run dev`. This requires the frontend webapp to have been build.

Startup configuration:

- ACCOUNTS_FILE must be set to the path of file containing the accounts
- DOMAINS, comma separated list of user domains
- BASE_PATH (optional) if the application needs to run in a subfolder https://example/change/password

## End to end tests

At the root, `npm run cypress:run` runs the unit tests with electron. To develop, run `npm run cypress:open`, it will open the cypress console.

## Docker

There is a `Dockerfile` provided that build the frontend and the backend to create an image that you can use.

The script `build.sh` performs the Docker build and runs the tests.

To run the server as a docker container change the volume and ACCOUNT_FILE accordingly.
`DOMAINS` is used to validate the email field. It should list all your users domain.

```
docker run                            \
  -p 8080:8080                        \
  -v $(pwd)/server/fixtures:/data     \
  -e ACCOUNTS_FILE=/data/accounts.txt \
  -e DOMAINS=test.local               \
  callmemagnus/password-updater
```

## TODOs

- add reset token generation tool
