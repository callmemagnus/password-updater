echo "BASE_PATH: $BASE_PATH"

function log_and_exit {
  echo "$1"
  exit
}

echo "$BASE_PATH" | grep -e "^/"
if [ $? != 0 ]; then
    BASE_PATH="/$BASE_PATH"
fi

rm -rf /public
NEW_DIRECTORY="/public$BASE_PATH"

echo "Change the base path to ${NEW_DIRECTORY}"
echo "Creating ${NEW_DIRECTORY}"
mkdir -p "$NEW_DIRECTORY"                       || log_and_exit "Failed to create ${NEW_DIRECTORY}"
cp -r /public-template/* "$NEW_DIRECTORY"       || log_and_exit "Failed to copy site template content"
cd "$NEW_DIRECTORY"                             || log_and_exit "Failed to change to ${NEW_DIRECTORY}"
sed -i "s%=\"/%=\"${BASE_PATH}/%gi" index.html
sed -i "s%//%/%gi" index.html

cat index.html

cd /app || log_and_exit "Failed to change to /public"

DEBUG=* node /app/server.js