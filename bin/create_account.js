#!/usr/bin/env node

const sha512crypt = require("sha512crypt-node").sha512crypt;
const randomstring = require("randomstring");

const template = (email, hash) => `${email}|{SHA512-CRYPT}${hash}`

const hash = (password, salt) => sha512crypt(password, salt);

const email = process.argv[2];
const password = process.argv[3];

if (!email || !password) {
  console.error(`Usage:
  ${process.argv[1]} {email} {password}
  `)
  process.exit(1)
}

console.log(`Provided data:
email: ${email}
password: ${password}
`)

console.log(template(email, sha512crypt(password, randomstring.generate(16))));