FROM node:12-alpine as frontend

WORKDIR /usr/src/app
COPY application .
RUN \
  npm ci && \
  npm run build


FROM node:12-alpine as backend

WORKDIR /usr/src/app
COPY domain .
RUN \
  npm install && \
  npm run prestart

FROM node:12-alpine
WORKDIR /app

ENV NODE_ENV="production"

COPY --from=frontend /usr/src/app/public /public-template
COPY --from=backend /usr/src/app/build /app
COPY domain/package.json domain/package-lock.json /app/

RUN \
  cd /app && \
  npm ci

EXPOSE 8080
VOLUME /data

COPY bin/docker_entrypoint.sh .

CMD ["/bin/sh", "/app/docker_entrypoint.sh"]
