import path from "path";
import Hapi, { Request, ResponseToolkit } from "@hapi/hapi";
import Joi from "joi";
import Inert from "@hapi/inert";
import Debug from "debug";
import Boom from "boom";

import log from "./log";
import login from "./routes/login";
import update from "./routes/update";

import { accountFile, basePath, host, port } from "./configuration";
import { authorizationScheme } from "./token_authentication";
import { canReadWrite } from "./infrastructure/file";

const debug = Debug("server");

const pathToStatic = path.resolve(
  __dirname,
  process.env.NODE_ENV === "production" ? "../public" : "../../application/public"
);

// Create a server with a host and port
const server = Hapi.server({
  host,
  port,
  routes: {
    files: {
      relativeTo: pathToStatic,
    },
  },
});

process.title = "passwordServer";

process.on("unhandledRejection", (err) => {
  log.error(err as string);
  process.exit(1);
});

process.on("SIGUSR1", async function () {
  await server.stop();
  process.exit(0);
});

const serveStaticFiles: Inert.RequestHandler<string> = (request) => {
  const buildUrl = (filePath: string) => `${basePath ? basePath.slice(1) + "/" : ""}${filePath}`;
  let requestPath = request.params.filename;

  // url is either
  // <filename> or
  // <basePath without first />
  if (basePath) {
    debug('Static -- request before basePath "%s"', basePath);
    requestPath = requestPath.replace(`${basePath.slice(1)}`, "");
  }

  if (requestPath.startsWith("/")) {
    requestPath = requestPath.slice(1);
  }

  debug('Static file request "%s"', requestPath);
  if (requestPath === "" || ["login", "update"].includes(requestPath)) {
    return buildUrl("index.html");
  }

  if (/(js|css|map|json)$/.test(requestPath)) {
    return buildUrl(requestPath);
  }

  Boom.notFound("Static file not found");
  return "";
};

// Start the server
async function start() {
  await server.register(Inert);

  server.validator(Joi);

  server.auth.scheme("custom", authorizationScheme);
  server.auth.strategy("token", "custom");

  server.route(
    [login, update].map((route) => {
      route.path = basePath + route.path;
      return route;
    })
  );

  server.route({
    method: "GET",
    path: basePath + "/api/ping",
    handler: (request: Request, h: ResponseToolkit) => {
      return h.response().code(200);
    },
    options: {
      auth: "token",
    },
  });

  // serving the site
  server.route({
    method: "GET",
    path: "/{filename*}",
    // handler: (request, h) => h.file(serveStaticFiles(request))
    handler: {
      file: serveStaticFiles,
    },
  });

  try {
    canReadWrite(accountFile);
  } catch (error) {
    log.error("process.env.ACCOUNTS_FILE is either not defined or not accessible");
    log.error(error.message ? error.message : error);
    return;
  }

  try {
    await server.start();
  } catch (err) {
    log.error(err);
    process.exit(1);
  }

  log.info(`Server running at ${server.info.uri}`);
}

start();
