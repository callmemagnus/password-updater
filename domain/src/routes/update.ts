import { AuthCredentials, Request, RequestAuth, ResponseToolkit, ServerRoute } from "@hapi/hapi";
import Debug from "debug";
import Boom from "boom";
import Joi from "joi";

import log from "../log";
import accounts from "../accounts";
import { accountFile } from "../configuration";

const debug = Debug("spu:server:routes:update");

interface Credentials extends AuthCredentials {
  readonly username: string;
}

interface RequestAuthUsername extends RequestAuth {
  credentials: Credentials;
}

interface RequestWithToken extends Request {
  auth: RequestAuthUsername;
}

interface UpdateRequest extends RequestWithToken {
  payload: {
    newPassword: string;
  };
}

const update = (request: UpdateRequest, h: ResponseToolkit) => {
  const username = request.auth.credentials.username;
  const newPassword = request.payload.newPassword;

  if (!username || !newPassword) {
    return h.response().code(400);
  }

  debug("updating password for %s", username);

  const result = accounts(accountFile).updatePassword(username, newPassword);

  if (result) {
    log.info(`Successful password update of ${username}.`);
    return h.response().code(200);
  }

  log.info(`Failed to update password update of ${username}.`);
  return Boom.internal("Could not update password");
};

export default {
  method: "POST",
  path: "/api/password",
  handler: update,
  options: {
    auth: "token",
    validate: {
      payload: Joi.object({
        newPassword: Joi.string().min(8).required(),
      }),
      // failAction: async (request, h, err) => {
      //   if (process.env.NODE_ENV === "production") {
      //     // In prod, log a limited error message and throw the default Bad Request error.
      //     console.error("ValidationError:", err?.message);
      //     throw Boom.badRequest(`Invalid request payload input`);
      //   } else {
      //     // During development, log and respond with the full error.
      //     console.error(err);
      //     throw err;
      //   }
      // },
    },
  },
  // }
  //   pre: [{
  //     method: preValidateToken,
  //     assign: 'email'
  //   }, {
  //     method: prePassword,
  //     assign: 'password'
  //   }]
  // }
} as ServerRoute;
