import Boom from "boom";
import Debug from "debug";
import Joi from "joi";
import { Request, ResponseToolkit, ServerRoute } from "@hapi/hapi";

import { LoginResult } from "../api";
import { addBadIp, isIPBanned } from "../infrastructure/badIps";
import accounts from "../accounts";
import { accountFile, usernameValidator } from "../configuration";
import { createToken } from "../jwt";
import log from "../log";
import extractIpFromRequest from "../extractIpFromRequest";

const debug = Debug("spu:server:routes:login");

debug("__dirname: %s", __dirname);

interface LoginRequest extends Request {
  payload: {
    username: string;
    password: string;
  };
}

const login = (request: LoginRequest, h: ResponseToolkit) => {
  const ip = extractIpFromRequest(request);

  if (isIPBanned(ip)) {
    throw Boom.unauthorized();
  }

  const payload = request.payload;

  const isValid = accounts(accountFile).checkPassword(payload.username, payload.password);

  if (isValid) {
    log.info(`Successful login of ${payload.username}.`);
    debug('Provided password for user "%s" is correct.', payload.username);
    return h.response({
      token: createToken(payload.username, ip),
    } as LoginResult);
  } else {
    log.error(`Failed to login by ${payload.username}`);
    debug('Provided password for user "%s" was wrong.', payload.username);
    addBadIp(ip);
    throw Boom.unauthorized();
  }
};

const requestSchema = Joi.object({
  username: usernameValidator.required(),
  password: Joi.string().required(),
});

const responseSchema = Joi.object({
  token: Joi.string(),
});

export default {
  method: "POST",
  path: "/api/login",
  handler: login,
  options: {
    cors: true,
    payload: {
      output: "data",
      parse: true,
      allow: "application/json",
    },
    validate: {
      payload: requestSchema,
    },
    response: {
      schema: responseSchema,
    },
  },
} as ServerRoute;
