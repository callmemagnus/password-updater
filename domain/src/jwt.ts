import jwt from "jsonwebtoken";
import randomstring from "randomstring";

const secret = randomstring.generate(20);

export function createToken(email: string, ip: string, expiresIn = "10m"): string {
  return jwt.sign(
    {
      email,
      ip,
    },
    secret,
    {
      expiresIn,
    }
  );
}

export function verifyToken(token: string): Record<string, string> {
  return jwt.verify(token, secret) as Record<string, string>;
}
