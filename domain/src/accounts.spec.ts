import test from "tape";
import randomstring from "randomstring";
import fs from "fs";
import path from "path";

import accounts from "./accounts";

const accountsFile = "../fixtures/accounts.txt";

console.log(__dirname, path.resolve(__dirname, accountsFile));

fs.writeFileSync(
  path.resolve(__dirname, accountsFile),
  fs.readFileSync(path.resolve(__dirname, accountsFile) + ".orig", "utf-8"),
  "utf-8"
);

test("checkPassword -- checking when user and password are correct", (assert) => {
  const result = accounts(path.resolve(__dirname, accountsFile)).checkPassword("test@test.local", "test43");
  assert.isEqual(result, true, "User exists and password is correct.");
  assert.end();
});

test("checkPassword -- checking when user is correct but password is wrong", (assert) => {
  const result = accounts(path.resolve(__dirname, accountsFile)).checkPassword("test@test.local", "test43asfsdf");
  assert.isEqual(result, false, "Check failed.");
  assert.end();
});

test("checkPassword -- checking when user is not is db", (assert) => {
  const result = accounts(path.resolve(__dirname, accountsFile)).checkPassword("non.existing@test.local", "test43");
  assert.isEqual(result, false, "Check failed.");
  assert.end();
});

test("updatePassword -- updating the password of an existing user", (assert) => {
  const newPassword = randomstring.generate(23);
  const email = "update.me@test.local";
  const result = accounts(path.resolve(__dirname, accountsFile)).updatePassword(email, newPassword);

  assert.isEqual(result, true, "Update password worked.");
  assert.isEqual(
    accounts(path.resolve(__dirname, accountsFile)).checkPassword(email, newPassword),
    true,
    "Password was checked to be correct."
  );
  assert.end();
});

test("updatePassword -- updating the password of a user not in the db", (assert) => {
  const result = accounts(path.resolve(__dirname, accountsFile)).updatePassword("non.existing@test.local", "test43");

  assert.isEqual(result, false, "Change should be rejected");
  assert.end();
});
