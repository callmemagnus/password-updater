import Debug from "debug";
import Boom from "boom";

import { verifyToken } from "./jwt";
import { ServerAuthSchemeObject } from "@hapi/hapi";
import extractIpFromRequest from "./extractIpFromRequest";

const debug = Debug("app:authorization");

export function authorizationScheme(): ServerAuthSchemeObject {
  return {
    authenticate: (request, h) => {
      const ip = extractIpFromRequest(request);
      try {
        const payload = verifyToken(request.headers.authorization.substr(7));
        debug("verify: %o", payload);
        if (payload && payload.ip === ip) {
          return h.authenticated({
            credentials: {
              username: payload.email,
            },
          });
        }
      } catch (e) {
        debug(e.message);
      }
      throw Boom.unauthorized(null, "token");
    },
  };
}
