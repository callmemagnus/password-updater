import { encrypt } from "unixcrypt";
import randomstring from "randomstring";
import Debug from "debug";

import { read, write } from "./infrastructure/file";

const debug = Debug("spu:server:accounts");

export interface Accounts {
  checkUsername: (email: string) => boolean;
  checkPassword: (email: string, password: string) => boolean;
  updatePassword: (email: string, password: string) => boolean;
}

export default function (filename: string): Accounts {
  return {
    checkUsername: (email: string) => {
      debug("Checking username (%s)", email);

      const data = read(filename);
      const filter = new RegExp(`^${email}`, "img");

      return !!filter.exec(data);
    },
    checkPassword: (email: string, password: string) => {
      debug("Checking password of %s", email);

      const data = read(filename);
      const filter = new RegExp(`^${email}\\|{SHA512-CRYPT}\\$6\\$([^$]+)\\$(.+)$`, "img");

      const result = filter.exec(data);

      if (!result) {
        return false;
      }

      debug("User %s is present in the database.", email);

      const salt = result[1];
      const hash = result[2];

      // console.log(sha512crypt(password, salt))
      // console.log(hash, salt);

      return encrypt(password, `$6$${salt}`) === `$6$${salt}$${hash}`;
    },
    updatePassword: (email: string, password: string) => {
      debug("Updating password for %s", email);
      const data = read(filename);

      const findFilter = new RegExp(`^${email}\\|`, "img");

      if (!findFilter.test(data)) {
        return false;
      }

      // backup
      // fs.writeFileSync(`${path.resolve(__dirname, filename)}.${new Date().toISOString()}.bak`,
      //   data,
      //   "utf-8"
      // );

      const hash = encrypt(password, `$6$${randomstring.generate(16)}`);

      const newValue = data.replace(new RegExp(`^(${email}\\|.*)$`, "igm"), `${email}|{SHA512-CRYPT}${hash}`);

      write(filename, newValue);

      return true;
    },
  };
}
