import winston from "winston";

export default winston.createLogger({
  level: process.env.NODE_ENV === "production" ? "info" : "silly",
  //format: winston.format.json(),
  format: winston.format.simple(),
  defaultMeta: { app: "password-updater", timestamp: Date.now() },
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log`
    // - Write all logs error (and below) to `error.log`.
    //
    // new winston.transports.File({ filename: "error.log", level: "error" }),
    // new winston.transports.File({ filename: "combined.log" })
    new winston.transports.Console(),
  ],
});
