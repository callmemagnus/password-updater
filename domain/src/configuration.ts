import Joi from "joi";

import cliVariables from "./infrastructure/configuration";

export const host = cliVariables.host || "0.0.0.0";
export const port = cliVariables.port || 8080;
export const basePath = cliVariables.basePath || "";
export const accountFile = cliVariables.accountPath;
export const usernameValidator = Joi.string().pattern(new RegExp(`^.*@(${cliVariables.domains.join("|")})$`));
export const adminUser = cliVariables.adminUser;
