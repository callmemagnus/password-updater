import log from "../log";

interface BadActivity {
  ip: string;
  timestamp: number;
}

let badIps: Record<string, number> = {};
let badActivity: BadActivity[] = [];
let cleaning = false;

const THRESHOLD = 3;
const DELETE_OLDER_THAN = 10 * 24 * 60 * 60 * 1000;
const ROLLING_PERIOD = 6 * 60 * 60 * 1000;

function cleanUp() {
  if (cleaning) {
    return;
  }
  cleaning = true;
  const now = Date.now();

  const deleteOlderThen = now - DELETE_OLDER_THAN;

  badActivity = badActivity.filter(({ timestamp }) => deleteOlderThen < timestamp);
  cleaning = false;
}

function updateCounter() {
  const now = Date.now();

  const ignoreOlderThan = now - ROLLING_PERIOD;

  badIps = badActivity
    .filter(({ timestamp }) => ignoreOlderThan < timestamp)
    .reduce((acc, { ip }) => {
      acc[ip] ??= 0;
      acc[ip] = acc[ip] + 1;
      return acc;
    }, {} as Record<string, number>);
}
export function addBadIp(ip: string): void {
  cleanUp();
  log.info(`Adding ${ip} to bad behaving list.`);
  badActivity.push({
    ip,
    timestamp: Date.now(),
  });
}

export function isIPBanned(ip: string): boolean {
  updateCounter();
  if (!badIps[ip]) {
    return false;
  }
  log.info(`${ip} is blocked due to bad behavior.`);
  return badIps[ip] >= THRESHOLD;
}
