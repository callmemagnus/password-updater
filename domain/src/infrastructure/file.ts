import fs from "fs";
import Debug from "debug";

const debug = Debug("spu:infrastructure:file");

export function canReadWrite(filename: string): boolean {
  try {
    fs.accessSync(filename, fs.constants.W_OK | fs.constants.R_OK);
    return true;
  } catch (e) {
    return false;
  }
}

export function read(filename: string): string {
  debug("Reading %s", filename);
  try {
    return fs.readFileSync(filename, "utf-8");
  } catch (e) {
    debug("There was an issue reading %s: %s", filename, e);
    throw e;
  }
}

export function write(filename: string, content: string): boolean {
  debug("Writing content in %s", filename);
  try {
    fs.writeFileSync(filename, content, "utf-8");
    return true;
  } catch (e) {
    debug("There was an issue writing %s: %s", filename, e);
    throw e;
  }
}
