import path from "path";
import Debug from "debug";

const debug = Debug("spu:configuration");

const { DOMAINS, ACCOUNTS_FILE, ADMIN_USER, HOST, PORT, BASE_PATH } = process.env;

if (!DOMAINS) {
  debug("DOMAINS environment variable is missing.");
  throw new Error("DOMAINS env variable is missing");
}

if (!ACCOUNTS_FILE) {
  debug("ACCOUNTS_FILE environment variable is missing.");
  throw new Error("ACCOUNTS_FILE env variable is missing");
}

debug(
  "DOMAINS are %s\nACCOUNTS_FILE is %s\nADMIN_USER is %s\nBASE_PATH is %s",
  DOMAINS,
  path.resolve(__dirname, "..", ACCOUNTS_FILE),
  ADMIN_USER,
  BASE_PATH
);

export default {
  domains: DOMAINS.split(","),
  accountPath: path.resolve(__dirname, "..", ACCOUNTS_FILE),
  adminUser: ADMIN_USER,
  host: HOST,
  port: PORT,
  basePath: BASE_PATH || "",
};
