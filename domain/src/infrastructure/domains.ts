import Debug from "debug";

const debug = Debug("spu:infrastructure:domains");

const domains = process.env.DOMAINS;

if (!domains) {
  debug("You must define `process.env.DOMAINS");
  process.exit(1);
}

export default domains.split(",");
