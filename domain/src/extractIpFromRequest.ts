import { Request } from "@hapi/hapi";

export default function (request: Request): string {
  const xFF = request.headers["x-forwarded-for"];
  return xFF ? xFF.split(",")[0] : request.info.remoteAddress;
}
