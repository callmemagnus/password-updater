describe("language", () => {
  beforeEach(() => {
    sessionStorage.clear();
    cy.visit("/");
  })

  it('should start in english', () => {
    cy.get('h1').contains('Login');
  })

  it('should switch langange to french', () => {
    cy.get('button.languages--language').eq(0).click();
    cy.get('h1').contains('Connexion');
  })
})