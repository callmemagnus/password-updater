describe("login", () => {
  beforeEach(() => {
    sessionStorage.clear();
    cy.visit("/");
  });

  it("should enable a known user to connect with correct credentials", () => {
    cy.get("[data-test=login]").click();
    cy.get("input[type=email]").type("test@test.local");
    cy.get("input[type=password]").type("test43");
    cy.get("button[type=submit]").click();
    cy.wait(1000);
    cy.url().should("include", "/update");
  });

  it("should fail when a bad password is provided", () => {
    cy.get("[data-test=login]").click();
    cy.get("input[type=email]").type("test@test.local");
    cy.get("input[type=password]").type("wrong-password");
    cy.get("button[type=submit]").click();
    cy.wait(1000);
    cy.get("[data-test=bad-password]").should("be.visible");
  });
});
