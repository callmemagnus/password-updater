describe("update", () => {
  beforeEach(() => {
    cy.visit("/");
    sessionStorage.clear();
  });

  const login = (cy, user, password) => {
    cy.get("[data-test=login]").click();
    cy.get("input[type=email]").type(user);
    cy.get("input[type=password]").type(password);
    cy.get("button[type=submit]").click();
    cy.wait(1000);
  };

  it("should update the password", () => {
    login(cy, "update1@test.local", "update1");
    cy.get("[data-test=password1]").type("abC0987654321");
    cy.get("[data-test=password2]").type("abC0987654321");
    cy.get("button[type=submit]").should("be.enabled");
    cy.get(".rules-copy").should("have.length", 1);
    cy.get(".rules-letters").should("have.length", 1);
    cy.get(".rules-special").should("have.length", 1);
    cy.get(".rules-len").should("have.length", 1);
    cy.get("button[type=submit]").click();
    cy.get("[data-test=success]").should("be.visible");
  });
});
