describe("password validations", () => {
  beforeEach(() => {
    cy.visit("/");
    sessionStorage.clear();
  });

  const login = (cy, user, password) => {
    cy.get("[data-test=login]").click();
    cy.get("input[type=email]").type(user);
    cy.get("input[type=password]").type(password);
    cy.get("button[type=submit]").click();
    cy.wait(1000);
  };

  it("should reject when new passwords do not match ", () => {
    login(cy, "test@test.local", "test43");
    cy.get("[data-test=password1]").type("abs");
    cy.get("[data-test=password2]").type("def");
    cy.get("button[type=submit]").should("be.disabled");
    cy.get(".rules-copy").should("have.length", 0);
  });

  it("should not accept password shorter than 8 characters", () => {
    login(cy, "test@test.local", "test43");
    cy.get("[data-test=password1]").type("abc");
    cy.get(".rules-len").should("have.length", 0);
  });

  it("should not accept password without a capital letter", () => {
    login(cy, "test@test.local", "test43");
    cy.get("[data-test=password1]").type("abc");
    cy.get("[data-test=password2]").type("abc");
    cy.get("button[type=submit]").should("be.disabled");
    cy.get(".rules-letters").should("have.length", 0);
  });

  it("should not accept password without a lower case letter", () => {
    login(cy, "test@test.local", "test43");
    cy.get("[data-test=password1]").type("ABC");
    cy.get("[data-test=password2]").type("ABC");
    cy.get("button[type=submit]").should("be.disabled");
    cy.get(".rules-letters").should("have.length", 0);
  });

  it("should show the validation status", () => {
    login(cy, "test@test.local", "test43");
    cy.get("[data-test=password1]").type("Aa");
    cy.get("[data-test=password2]").type("Aa");
    cy.get("button[type=submit]").should("be.disabled");
    cy.get(".rules-len").should("have.length", 0);
    cy.get(".rules-copy").should("have.length", 1);
    cy.get(".rules-letters").should("have.length", 1);
    cy.get(".rules-numbers").should("have.length", 0);
  });
});
