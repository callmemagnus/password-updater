describe("password field", () => {
  beforeEach(() => {
    sessionStorage.clear();
    cy.visit("/");
  });

  it("should be of type password by default", () => {
    cy.get("input[data-test=password]").type("test43");
    cy.get("input[data-test=password][type=password").should("exist");
    cy.get("input[data-test=password][type=text").should("not.exist");
    // cy.get("input[data-test=password]")
    //   .next()
    //   .click();
  });

  it("should toggle to type text", () => {
    cy.get("input[data-test=password]").type("test43");
    cy.get("input[data-test=password]").next().click();
    cy.get("input[data-test=password][type=text").should("exist");
    cy.get("input[data-test=password][type=password").should("not.exist");
  });
});
